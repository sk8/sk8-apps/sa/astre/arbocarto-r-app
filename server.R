# Define server logic required to draw a histogram
server <- function(input, output, session) {

  options(shiny.maxRequestSize=30*1024^2)

  data <- reactiveValues(
    SHAPE = NULL,
    Species_data = NULL,
    Pathogen_data = NULL,
    parcels_data = NULL,
    meteo_data = NULL,
    prev_control = NULL,
    introduction_pts = NULL,
    SHAPE_input = NULL,
    SHAPE_upload = NULL,
    SHAPE_stations = NULL,
    aggreagation_list = NULL,
    parcels_upload = NULL,
    meteo_upload = NULL,
    pushed_data = FALSE,
    mMov = NULL,
    referenceID_mobility = NULL
  )

  data_output <- reactiveValues(
    SHAPE = NULL,
    Species_data = NULL,
    Pathogen_data = NULL,
    parcels_data = NULL,
    meteo_data = NULL,
    mMov = NULL,
    prev_control = NULL,
    traj = NULL,
    traj_baseline = NULL,
    introduction_pts = NULL,
    selected_parcel = NULL##
  )

  curr_date <- reactiveValues(start = NULL,
                                     end = NULL)

  # system("java -version")

  #############

  # Auxiliary function - related to delete rows in renderDT
  shinyInput <- function(FUN, len, id, ...) {
    inputs <- character(len)
    for (i in seq_len(len)) {
      inputs[i] <- as.character(FUN(paste0(id, i), ...))
    }
    inputs
  }

  source("functions/conditions_reactive.R", local = TRUE)

  #############

  ### Initialization

  # Select study area and update SHAPE / PARCELS / METEO
  source("functions/select_study_area.R", local = TRUE)
  source("functions/species_modif.R", local = TRUE)
  # Display input maps
  source("functions/input_maps.R", local = TRUE)

  ### Simulations

  # Select simulation dates
  source("functions/simulation_dates.R", local = TRUE)
  # Define control measures
  source("functions/control.R", local = TRUE)
  # Define introductions
  source("functions/introduction.R", local = TRUE)
  # Choose the mobility pattern
  source("functions/mobility.R", local = TRUE)

  ### Run the model
  source("functions/runmodel.R", local = TRUE)
  source("functions/valueBoxes.R", local = TRUE)
  source("functions/plot_control_violine.R", local = TRUE)
  source("functions/epidemiological_curve.R", local = TRUE)
  source("functions/temporal_slider_output.R", local = TRUE)
  source("functions/map_output.R", local = TRUE)
  source("functions/output_select_parcel.R", local = TRUE)
  source("functions/dynamics_plot.R", local = TRUE)
  source("functions/meteodata_plot.R", local = TRUE)

  ###### Download data
  source("functions/download_rawresults.R", local = TRUE)

  ###### Upload data

  source("functions/upload_shapefile.R", local = TRUE)
  source("functions/upload_parcelsfile.R", local = TRUE)
  source("functions/upload_meteofile.R", local = TRUE)
  source("functions/download_templates.R", local = TRUE)
  source("functions/push_data.R", local = TRUE)

  observe_helpers(withMathJax = TRUE)
} # end server function
