## Aggregation functions

***
Except for the ids, the attributes of the aggregated parcels will be sum.

This approach is relevant for the environmental carrying capacities and human populations, but if you use weather station records for meteorological data, the average altitude and station allocation have to be re-estimated.
