## Parcels data strcture

***
The environmental dataset must be a csv or txt file using ';' as separator and '.' for decimal.
It should contain at least four columns with the following headers: ID; Kfix, Kvar and POP.
* The ID column must be either the unique identifier the studied parcels (identifiers must be the same as in the shapefile).
* The Kfix column should contain a fixed environmental load of larvae associated with anthropogenic activity. It should be formulated as a maximum number of larvae or pupae that can be supported by the parcel in the anthropogenic larval breeding sites.
* The Kvar column should contain a variable environmental load of larvae associated with natural activity. It should be formulated as a maximum number of larvae or pupae that can be supported by the parcel in the natural larval breeding sites. This environmental load will vary over time with the meteorological conditions.
* The POP columns is the human population size in a given parcel. It should be formulated in persons.

***
**Optional column: If you use meteorological data from weather stations.**
You must associate a station with each parcel and provide the altitude of the station and the difference with the average altitude of the parcel. This information will be used to estimate temperatures and precipitation at parcel level, based on records from the stations. The following columns are expected:
* STATION: unique identifier of the associated station
* ALT_M: altitude of the station in meters
* DIFF_ALT: difference between average altitude of the parcel and the altitude of the associated meteorological station: $$ALT_{parcel} - ALT_{station}$$
