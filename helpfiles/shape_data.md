## Spatial polygons attributes

***
The dataset must be composed of a minimum of 4 files: .shp, .dbf, .prj, .shx.
Polygon attributes must inform the unique identifier of each parcel.
These identifiers must be the same as in the environmental dataset.

All the data required in the environmental dataset can be supplied as attributes and specified to build the environmental dataset template.
