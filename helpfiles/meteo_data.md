## Meteorological data strcture

***
The meteorological dataset must be a csv or txt file using ';' as separator and '.' for decimal.
It should contain at least four columns with the following headers: DATE; ID; RR and TP.
* The DATE column must be formatted as follow: YYYY-MM-DD (ex: 2023-12-31).
* The ID column must be either the unique identifiers of the parcels (identical to those of the shapefile and the environmental dataset) or the unique identifier of meteorological stations (identical to the STATION column in the environmental dataset).
* The RR column must contain the daily precipitation amount in a given parcel, it should be formulated in millimeter.
* The TP columns is the average daily temperature in a given parcel. It should be formulated in degrees.
