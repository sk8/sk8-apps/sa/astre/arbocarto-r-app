# Load and if necessary install packages
# list of packages required
list.of.packages <- c("shiny",
                      "shinydashboard", "shinyWidgets", "shinyhelper", "shinyjs","shinyscreenshot", "spsComps", "DT", "shinyalert",
                      "arbocartoR",
                      "sf", "terra", "ggplot2", "dygraphs", "raster",
                      "exactextractr", # only loaded if used
                      "leaflet", "htmltools","glue",
                      "ggpubr", "tidyterra", "lubridate", "tools",
                      "elevatr",
                      "furrr",
                      "slippymath"
                      )

source('functions/check_packages.R', local = TRUE)
check_packages(list.of.packages)
# install.packages("remotes")
# remotes::install_github("rspatial/geodata")

library("shiny")
library("shinydashboard")
library("shinyWidgets")
library("shinyhelper")
library("shinyjs")
library("shinyscreenshot")
library("spsComps")
library("DT")
library("magrittr")
library("sf")
library("terra")
library("tidyterra")
library("raster")
library("ggplot2")
library("dygraphs")
library("leaflet")
library("htmltools")
library("glue")
library("ggpubr")
library("lubridate")
library("tools")
library("elevatr")
library("furrr")
library("slippymath")
library("arbocartoR")

## only used to import data
# library("KrigR")
library("geodata")

#### Source function

# App Structure function

source('body/presentation.R', local = TRUE)
source('body/content/tabpanel.R', local = TRUE)
source('body/content/tab1_howto.R', local = TRUE)
source('body/content/tab2_tool.R', local = TRUE)
source('body/content/tab3_import.R', local = TRUE)
source('body/about.R', local = TRUE)

source('header/header_ui.R', local = TRUE)
source('body/sidebar_ui.R', local = TRUE)
source('body/body_ui.R', local = TRUE)

