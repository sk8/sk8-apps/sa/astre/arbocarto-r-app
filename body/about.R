#############
#############
### About ###
#############
#############

tabItemAbout <- function(){

  tabItem("ABOUT",
          markdown("
# About

### Authors
Pachka Hammami<sup>1</sup>, Renaud Marti<sup>2</sup>,  Annelise Tran<sup>2</sup>, Andrea Apolloni<sup>1</sup> and  Elena Arsevska<sup>1</sup></font>

### Collaborators
Ewy Ortega<sup>1,3</sup>, Maxime Lenormand<sup>2</sup>, Marie Demarchi<sup>4</sup>

### Affiliations
<sup>1</sup> UMR ASTRE, CIRAD, INRAE, Université de Montpellier, 34000 Montpellier, France

<sup>2</sup> UMR TETIS, INRAE, Cirad, AgroParisTech, CNRS, Maison de la télédetection, 34000 Montpellier, France

<sup>3</sup> DGAL, Direction Générale de L'alimentation, 251 Rue de Vaugirard, 75015 Paris, France

<sup>4</sup> Maison de la Télédétection, Montpellier, France

### Contact:
Corresponding author: Arsevska Elena (elena.arsevska@cirad.fr)

</br>

### Funding:
arbocartoR was developed in the frame of the MOOD project: MOnitoring Outbreaks for Disease surveillance in a data science context.
This work has received funding from the European Union's Horizon 2020 research and innovation programme under Grant Agreement MOOD N° 874850.

</br>

### Origins:
ARBOCARTO: The mosquito population dynamics model was initially developed for the ARBOCARTO tool.

### Licence:
Cirad is the owner of the arbocartR tool, which is distributed under the CECILL C free licence.
")


  )
}
