
### Upload parcels
observeEvent(input$fileparcels,{

  file <- input$fileparcels
  ext <- file_ext(file$datapath) ## tools package
  req(file)

  if(is.null(file))
    return(NULL)

  file_contents <- read.table(file$datapath,
                              sep = ";",
                              dec = ".",
                              quote = "",
                              header=TRUE,
                              colClasses = "character")

  required_columns <- c("ID", "DIFF_ALT", "Kfix", "Kvar", "POP")
  column_names <- colnames(file_contents)

  if(any(is.na(file_contents$Kfix)))
  {
    shinyalert("Column content Error","missing values in the Kfix column",type="error")
    returnValue()
  }

  if(any(is.na(file_contents$Kvar)))
  {
    shinyalert("Column content Error","missing values in the Kvar column",type="error")
    returnValue()
  }

  if(any(!(required_columns %in% column_names)))
  {
    shinyalert("Column Error",
               paste(c("columns are missing:" , required_columns[!(required_columns %in% column_names)]), collapse = " "),
               type="error")
    returnValue()
  }

  validate(
    need(!any(is.na(file_contents$Kfix)), message = "missing values in the Kfix column"),
    need(!any(is.na(file_contents$Kvar)), message = "missing values in the Kvar column"),
    need(ext %in% c('xls', 'csv', 'txt', 'xlsx'), message = "Incorrect file type"),
    need(required_columns %in% column_names, message = paste(paste(required_columns[!(required_columns %in% column_names)], "column is missing"), collapse = "\n "))
  )

  data$parcels_upload <- file_contents
})

output$parcels_upload_table <- renderDT({
  data$parcels_upload
})

output$summary_parcel_upload <- renderText({
  if(!is.null(data$parcels_upload)){
    message_to_display <- paste("You have uploaded data for", nrow(data$parcels_upload), "patchs.")

    if(!is.null(data$SHAPE_upload) & !is.null(input$ID_col)){

      parcels_shape <- unlist(data$SHAPE_upload[[input$ID_col]])

      if(setdiff(data$parcels_upload$ID,  parcels_shape) %>% length %>% is_greater_than(0))
        message_to_display %<>%  paste(., "\n",
          length(setdiff(data$parcels_upload$ID,
                         parcels_shape)),
              "patchs do not exists in the shapefile IDs.")

      if(setdiff(parcels_shape, data$parcels_upload$ID) %>% length %>% is_greater_than(0))
        message_to_display %<>%  paste(., "\n",
          length(setdiff(parcels_shape,data$parcels_upload$ID)),
                                     "patchs exists in the shapefile but not in the parcel dataset.")

      }

    return(message_to_display)
  }
})

observeEvent(input$clear_parcels, {
  data$parcels_upload <- NULL
})
